App = Ember.Application.create();

App.Router.map(function() {
  // put your routes here
});

App.ApplicationAdapter = DS.FixtureAdapter;

App.Contact = DS.Model.extend({
	name: DS.attr('string'),
	email: DS.attr('string')
});

App.Contact.FIXTURES = [
	{id: 1, name: 'Fani Hamdani', email: 'fani@email.com'},
	{id: 2, name: 'Andre Pakpahan', email: 'andre.pph@email.net'},
	{id: 3, name: 'Andy Wiranata', email: 'andy@email.org'},
	{id: 4, name: 'Darwin Cuputra', email: 'darwin@email.net'},
	{id: 5, name: 'Andre Kurniawan', email: 'andre.k@email.com'},
	{id: 6, name: 'Yuki Buwana', email: 'yuki.b@email.org'},
	{id: 7, name: 'Andri Khrisharyadi', email: 'andri.k@email.com'},
	{id: 8, name: 'Agus Winarno', email: 'agus.w@email.com'},
	{id: 9, name: 'Okky Hendriansyah', email: 'okky.h@email.net'}
];

App.IndexRoute = Ember.Route.extend({
  model: function() {
	  return this.store.find('contact');
  }
});

App.IndexController = Ember.ArrayController.extend({
	filter: '',
	
	filteredContent: function() {
		var filter = this.get('filter');
	    var rx = new RegExp(filter, 'gi');
	    var contacts = this.get('model');

	    return contacts.filter(function(c) {
	      return c.get('name').match(rx) || c.get('email').match(rx);
		});

	}.property('filteredContent', 'filter')
});
